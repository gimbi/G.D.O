G.D.O (prononcer JIDO) est une interface qui a vu le jour suite à la mise en place de <a href="https://github.com/monptitnuage/J.A.R.V.I.S."><b>JARVIS</b></a> et de mon miroir connecté.

Je l'ai entièrement développé afin de m'apporter une aide dans la gestion des différentes options de JARVIS. Je vous met ci-dessous les possibilités fournies  par G.D.O :
- Déclencher manuellement certaines actions de JARVIS + d'autres actions propres à G.D.O
- Gestion de mon stock alimentaire et ménagé.
- Ajout et suppression d'aliment ou produit ménagé présent dans le stock.
- Accèder à différentes recettes de cuisine.
- Minuteur.
- Gestion de mon agenda (ajout et suppression).
- Ajouter un rappel.
- Supprimer un rappel.
- Allumer/éteindre les lampes de la cuisine.

Auparavant, pour utiliser la plupart des options cités ci-dessus, je devais faire des modifications directement dans des fichiers texte ce qui n'était pas du tous optimisé. Avec l'aide G.D.O je peux désormais faire tous ça plus facilement.

Je trouve son utilisation vraiment pratique, c'est pourquoi je vous met à disposition tous les codes nécessaires pour son fonctionnement.
Si vous le souhaitez vous pouvez avoir un aperçu de ce que ça donne <a href="http://monptitnuage.fr/index.php?article151/ma-domotique-personnelle"><b>ici</b></a> (aller directement à 10:00min si vous voulez zapper la partie sur JARVIS).

ATTENTION !!
<ul>
<li>Le dossier curl.tar.gz doit être décompressé et mis à la racine du dossier GIDO, vous en avais besoin pour allumer/éteindre les lampes.</li>
<li>Le dossier fichierTexte.tar.gz contient les fichiers texte nécessaire au fonctionnement du calendrier, rappel, et gestion du stock alimentaire et ménagé. Je vous les met à disposition pour exemple.
